#!/bin/ksh

typeset array[2]

array[0]=$(mocp -i | grep -e Artist | sed s/^Artist:" "//g)
array[1]=$(mocp -i | grep -e SongTitle | sed s/^SongTitle:" "//g)

if [[ -n "${array[0]}" && "${array[1]}" ]]; then
	print "${array[0]} - ${array[1]}"
fi

